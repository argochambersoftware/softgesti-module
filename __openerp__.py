# -*- coding: utf-8 -*-
# Module SoftGestió by Pablo

{
    'name': 'SoftGestió',
    'version': '1.0',
    'author': 'Pablo Blanco Celdrán',
    'sequence': 1,
    'summary': 'Software per gestió',
    'description': """
SoftGestió
==========
Ies Carles vallbona 2016
Projecte DUAL
    """,
    'website': '',
    'depends': [],
    'category': '',
    'data': [ 'softgestio_view.xml', 'reports.xml' ],
    'demo': [
        'demo/demo.xml'
    ],
    'installable': True,
}
