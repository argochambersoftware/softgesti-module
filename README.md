# Odoo 9.0 Module
## Soft-Gestió

Software management module.

Argochamber Interactive 2016 - Pablo 'sigmasoldi3r' Blanco

* * *

## Summary

+ Model
 - [App Class](#Table: Apps)
 - [Operating System Class](#Table: Apps)
 - [User Class](#Table: Apps)
 - [Developer Class](#Table: Apps)
+ Views
 - Must add.

## Model

TODO: Add model.

#### Table: Apps
`class soft_app`

#### Table: Operating Systems
`class soft_os`

#### Table: App Users
`class soft_user`

#### Table: App Developers
`class soft_dev`

## Views

TODO: Add views.

* * *

### Licensed under MIT

Copyright (c) 2016 Pablo Blanco Celdrán

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
