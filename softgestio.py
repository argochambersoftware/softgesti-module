# -*- coding: utf-8 -*-
# Module SoftGestió by Pablo

from openerp.osv import osv, fields


class soft_app(osv.osv):
    """Apps"""
    _name = 'softgestio.app'
    _columns = {
        'name': fields.char('Name', size=32, required=True, help='This is the name of the application'),
        'desc': fields.text('Description', help='A brief description of the application'),
        'dev_id': fields.many2one('softgestio.dev','Developer',required=True, help='The app\'s developer'),
        'release': fields.datetime('Release Date', help='The date that the app was released'),
        'downloads': fields.integer('Download Count', help='Total amount of downloads'),
        'user_ids': fields.many2many('softgestio.user','soft_user_app_rel','app_id','user_id','Users Downloaded', help='Users that have downloaded the app'),
        'os_ids': fields.many2many('softgestio.os','soft_os_app_rel','app_id','os_id','Operating Systems', help='Operating systems survey data'),
        'valid': fields.boolean('Is Valid', help='Checks if the app is validated')
    }
soft_app()


class soft_os(osv.osv):
    """Operating Systems"""
    _name = 'softgestio.os'
    _columns = {
        'name': fields.char('Name', size=32, required=True, help='This is the short name of the OS'),
        'full_name': fields.text('Full Name', help='This is the complete commercial name of the OS'),
        'brand': fields.selection( [('droid', 'Android'), ('ios', 'iOS'), ('win', 'Windows')], 'Family' ),
        'app_ids': fields.many2many('softgestio.app','soft_os_app_rel','os_id','app_id','Operating Systems', help='OS Coverage data')
    }
soft_os()

class soft_user(osv.osv):
    """App Users"""
    _name = 'softgestio.user'
    _columns = {
        'username': fields.char('Username', size=32, required=True, help='The unique username'),
        'partner_id': fields.many2one('res.partner', 'Associated Partner'),
        'full_name': fields.related('partner_id', 'name', type='char', string='Partner Name', size=64, readonly=True),
        'email': fields.related('partner_id', 'email', type='char', string='Partner E-Mail', size=64, readonly=True),
        'app_ids': fields.many2many('softgestio.app','soft_user_app_rel','user_id','app_id','Apps Downloaded', help='The apps that the user has downloaded')
    }
soft_user()

class soft_dev(osv.osv):
    """App Developers"""
    _name = 'softgestio.dev'
    _columns = {
        'name': fields.char('Name', size=128, required=True, help='This is the full name of the developer'),
        'id_number': fields.char('ID Card Number', size=9, required=True, help='This is the personal ID card number (DNI in spanish/catalan)'),
        'email': fields.char('EMail', size=512, help='Developer\'s e-Mail'),
        'app_ids': fields.one2many('softgestio.app', 'dev_id', 'Developed apps')
    }
soft_dev()
